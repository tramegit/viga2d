% → solve2d
% Calcula os deslocamentos de rotação dos nós da estrutura
%
% Dados de entrada:
%       Ks - matriz de rigidez global
%        F - vetor dos esforços
%       Rz - vetor das restriçõestrutura
%
% Dados de saída
%        d - vetor dos deslocamentos
% 
function [d] = solve2d(Ks, F, Rz)
  nnos=size(Rz,1);
  Ksr=Ks; %matriz global restringida
  Fr=F;   %vetor dos esforços restringido
    for i = 1:nnos
      if (Rz(i) == 1)
        Fr(i)=0;
        Ksr(i,1:nnos)=0;
        Ksr(1:nnos,i)=0;
        Ksr(i,i)=1; % o valor na diagonal não pode ser zero
      endif
    endfor
    d = inv(Ksr) * (-1*Fr)
endfunction