
% → baresf
% Calcula o vetor dos esforços da barra 
% Apenas para carga uniformemente distribuída
%
% Dados de entrada:
%     q - carga uniformemente distribuída
%     L - comprimento da barra
%
% Dados de saída:
%     K - matriz de rigidez 2x2 da barra de viga
%
function [bf] = baresf(q, L)
  bf = [ q*L^2/12 ; -q*L^2/12 ]; 
endfunction