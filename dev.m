% → dev
% Diagrama de esforço cortante
% Monta o diagrama de esforços cortantes das barras na figura 1.
%
% Dados de entrada
%       xa - coordenada x do nó inicial da barra
%       xb - coordenada x do nó final da barra
%        q - carga uniformemente distribuída ao longo da barra
%     Mbar - Vetor com os esforços de momento fletor nos nós de extremidade da barra 
%
% Dado de saída
%       Gráfico figura 1.
%

function dev (xa, xb, q, Mbar)
 figure(1)
 L=xb-xa;
 Va=(q*L/2) + ((Mbar(1)+Mbar(2))/L);     %cortante inicial
 Vb=Va-(q*L);                            %cortante final
 ndot=100;                               %resolução do gráfico
 datax=zeros(1,ndot+1);
 datay=zeros(1,ndot+1); 

  for i = 1:ndot+1
      xi = (L/ndot)*(i-1);
      datax(i)=xa + xi;
      datay(i)=Va-(q * xi);
    endfor

 
 xlabel("Eixo X → Tramos")
 ylabel("Esforço Cortante")
 grid on
 line("xdata", [xa xb],"ydata", [0 0],"color", "black", "linewidth",3) 
 line("xdata", datax,"ydata", datay,"color", "red", "linewidth",1) 
 line("xdata", [xa xa],"ydata", [0 Va],"color", "red", "linewidth",1) 
 line("xdata", [xb xb],"ydata", [0 Vb],"color", "red", "linewidth",1) 
endfunction