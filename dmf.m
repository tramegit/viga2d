% → dmf
% Diagrama de momento fletor
% Monta o diagrama de momentos fletores das barras na figura 2.
%
% Dados de entrada
%       xa - coordenada x do nó inicial da barra
%       xb - coordenada x do nó final da barra
%        q - carga uniformemente distribuída ao longo da barra
%     Mbar - Vetor com os esforços de momento fletor nos nós de extremidade da barra 
%
% Dado de saída
%       Gráfico figura 2.
%
function dmf (xa, xb, q, Mbar)
 figure(2)
 L=xb-xa;
 Va=(q*L/2) + ((Mbar(1)+Mbar(2))/L);     %cortante inicial
 Vb=Va-(q*L);                            %cortante final
 ndot=100;                               %resolução do gráfico
 datax=zeros(1,ndot+1);
 datay=zeros(1,ndot+1); 

  for i = 1:ndot+1
      xi = (L/ndot)*(i-1);
      datax(i)=xa + xi;
      datay(i)=Mbar(1) - (Va * xi) + (q * xi^2 / 2);
    endfor

 
 xlabel("Eixo X → Tramos")
 ylabel("Momento Fletor")
 grid on
 line("xdata", [xa xb],"ydata", [0 0],"color", "black", "linewidth",3) 
 line("xdata", datax,"ydata", datay,"color", "red", "linewidth",1) 
 line("xdata", [xa xa],"ydata", [0 datay(1)],"color", "red", "linewidth",1) 
 line("xdata", [xb xb],"ydata", [0 datay(101)],"color", "red", "linewidth",1) 
endfunction