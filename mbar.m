% → mbar
% Calcula os momentos fletores equilibrados nas extremidades das barras
%
% Dados de entrada:
%       nbar - número da barras
%          K - matriz de rigidez local da barra 
%         bf - vetor dos esforços da barra
%          d - vetor global dos deslocamentos calculados
%
% Dados de saída
%        M - vetor com os valores dos momentos fletores finais
% 
function [M] = mbar(nbar, K, bf, d)
      dbar=zeros(1,2)';
      dbar(1)=d(nbar);
      dbar(2)=d(nbar+1);
      M=K*dbar+bf; 
  endfunction