# VIGA2D 
### Versão 21.10
![](viga2d.png)
Funções em [Octave](https://www.gnu.org/software/octave/index) para análise matricial de vigas contínuas utilizando o método da rigidez direta. O objetivo deste programa é demonstrar como é possível realizar a análise de vigas contínuas com apenas uma deslocabilidade de rotação por nó. A descrição de cada função se encontra no próprio arquivo de código-fonte.

## Características do programa

- Matriz de rigidez de barra com apenas uma coordenada de rotação por nó.
- Carga uniformemente distribuída ao longo de toda a barra.
- Sem limite de número de barras.
- Possível definir vão e carga para cada barra individualmente.


## Exemplo de uso
![](exemplo.png)
Sem necessidade de instalação, basta entrar na pasta aonde você salvou as funções e executar o arquivo **exemplo.m** dentro do [Octave](https://www.gnu.org/software/octave/index). 
Utilize o exemplo abaixo para calcular diferentes vigas apenas alterando para o número de barras com vãos e cargas desejados.

``` octave
%Exemplo 1 - Viga com 3 tramos iguais com mesma carga uniformemente distribuída

% Dados das barras (que neste exemplo são idênticas)
L=400;
I=24*40^3/12
E=2000;
q=0.03;

% matriz de rigidez das barras
k1=viga2d(L, E, I);
k2=viga2d(L, E, I);
k3=viga2d(L, E, I);

% Matriz de rigidez global (neste caso com 3 barras)
[Ks,nbar]=global2d(k1,k2,k3)

% Vetor dos esforços das barras
bf1=baresf(q, L);
bf2=baresf(q, L);
bf3=baresf(q, L);

% Vetor global dos esforços
F=vetoresf(bf1,bf2,bf3);

% Vetor das coordenadas de deslocamento restringidas 
nnos=nbar+1; % número de nós
Rz=rotzero(nnos,1); %restringindo apenas a coordenada 1

% Cálculo dos deslocamentos
clc
printf("Vetor dos Deslocamentos \n");
printf("----------------------- \n");
d = solve2d(Ks, F, Rz);

printf(" \n");
printf("Esforços finais na Barra 1 \n");
printf("-------------------------- \n");
m1=mbar(1,k1,bf1,d)
dev(0,L, q, m1) 
dmf(0,L, q, m1)

printf(" \n");
printf("Esforços finais na Barra 2 \n");
printf("-------------------------- \n");

m2=mbar(2,k2,bf2,d)
dev(L,2*L, q, m2) 
dmf(L,2*L, q, m2) 

printf(" \n");
printf("Esforços finais na Barra 1 \n");
printf("-------------------------- \n");

m3=mbar(3,k3,bf3,d)
dev(2*L,3*L, q, m3)
dmf(2*L,3*L, q, m3)
```


## Licença


[WTFPL](http://www.wtfpl.net/about/)

```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2021 Paulo C. Ormonde < contato@trameestruturas.com.br >

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

```


