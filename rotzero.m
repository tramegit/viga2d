% → rotzero
% Montagem de um vetor que indica as coordenadas de deslocamento restringidas 
%
% Dados de entrada:
%     nnos - número total de nós (número de barras + 1)
%     1, 5,...,n - apenas o número dos nós com rotação restringida
%
% Dados de saída:
%     Rz - Vetor dos nós restringidos (valor igual 1)
%

function [Rz] = rotzero(nnos, varargin)
  nr=length(varargin);
  Rz=zeros(1,nnos)';
    for i = 1:nr
      ri=varargin{i};
      Rz(ri)=1;
   endfor
endfunction