%Exemplo 1 - Viga com 3 tramos iguais com mesma carga uniformemente distribuída

% Dados das barras (que neste exemplo são idênticas)
L=400;
I=24*40^3/12
E=2000;
q=0.03;

% matriz de rigidez das barras
k1=viga2d(L, E, I);
k2=viga2d(L, E, I);
k3=viga2d(L, E, I);

% Matriz de rigidez global (neste caso com 3 barras)
[Ks,nbar]=global2d(k1,k2,k3)

% Vetor dos esforços das barras
bf1=baresf(q, L);
bf2=baresf(q, L);
bf3=baresf(q, L);

% Vetor global dos esforços
F=vetoresf(bf1,bf2,bf3);

% Vetor das coordenadas de deslocamento restringidas 
nnos=nbar+1; % número de nós
Rz=rotzero(nnos,1); %restringindo apenas a coordenada 1

% Cálculo dos deslocamentos
clc
printf("Vetor dos Deslocamentos \n");
printf("----------------------- \n");
d = solve2d(Ks, F, Rz);

printf(" \n");
printf("Esforços finais na Barra 1 \n");
printf("-------------------------- \n");
m1=mbar(1,k1,bf1,d)
dev(0,L, q, m1) 
dmf(0,L, q, m1)

printf(" \n");
printf("Esforços finais na Barra 2 \n");
printf("-------------------------- \n");

m2=mbar(2,k2,bf2,d)
dev(L,2*L, q, m2) 
dmf(L,2*L, q, m2) 

printf(" \n");
printf("Esforços finais na Barra 1 \n");
printf("-------------------------- \n");

m3=mbar(3,k3,bf3,d)
dev(2*L,3*L, q, m3)
dmf(2*L,3*L, q, m3)