% → vetoresf
% Montagem do Vetor Global dos Esforços
%
% Dados de entrada:
%     bf1, bf2, bf3,...,bfn - vetores dos esforços das barras
%
% Dados de saída:
%     F - Vetor global dos esforços
%

function [F] = vetoresf (varargin)
  nbar=length (varargin);
  F=zeros(1,nbar+1)'
    for i = 1:nbar
    Fi=varargin{i};
    F(i:i+1)=F(i:i+1) + Fi;
  endfor
endfunction