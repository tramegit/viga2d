% → matriz_global
% Monta a matriz global da viga contínua
% informar as matrizes de rigidez das barras na ordem correta
% Dados de entrada
%       K1, K2, K3,..., Kn  - matrizes de rigidez 
% Dado de saída
%       Ks - matriz de rigidez global não restringida
%
function [Ks,nbar] = global2d (varargin)
  nbar=length (varargin);
  Ks=zeros(nbar+1,nbar+1)
    for i = 1:nbar
    Ki=varargin{i};
    Ks(i:i+1,i:i+1)=Ks(i:i+1,i:i+1) + Ki;
  endfor
endfunction